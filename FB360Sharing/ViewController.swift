//
//  ViewController.swift
//  FB360Sharing
//
//  Created by 2Mac on 09.05.2019.
//  Copyright © 2019 2Mac. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onShareButtonClick(_ sender: Any) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/"
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let resultDate = formatter.string(from: date)
        formatter.dateFormat = "HH-mm-ss"
        let resultTime = formatter.string(from: date)
        let name = "img_" + resultTime + "_" + resultDate
        
        let filePath = path + name + "fb" + ".jpeg"
        
        let testpath = Bundle.main.url(forResource: "testPhoto", withExtension: "jpeg") // image with metadata
        
        let dataTestOne = try? Data(contentsOf: testpath!)
        
        let fileManager = FileManager()
        fileManager.createFile(atPath: filePath, contents: dataTestOne, attributes: [kCFURLFileProtectionKey as FileAttributeKey: kCFURLFileProtectionComplete])
        
        try? PHPhotoLibrary.shared().performChangesAndWait {
            PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: URL(fileURLWithPath: filePath))
        }
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        let assets: PHFetchResult<PHAsset> = PHAsset.fetchAssets(with: options)
        var lastAsset: PHAsset?
        if assets.count > 0 {
            lastAsset = assets.lastObject     // This photo in gallery still has metadata
        }
        
        
        let photo = FBSDKSharePhoto(photoAsset: lastAsset, userGenerated: true)
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = FBSDKShareDialogMode.automatic
        dialog.show()
    }
}

